import {
    it,
    describe,
    expect
} from 'vitest';
import { 
    Button 
} from './button'
import {
    render,
    screen
} from '@testing-library/react';
import { Link } from 'lucide-react';
import '@testing-library/jest-dom/vitest';

describe('button component', async () => {

    describe('check snapshot', async () => {

        it('default button', async () => {
            render(<Button>Button</Button>);
            const btn = screen.getByRole('button');
            expect(btn).toMatchSnapshot();
        });

        it('destructive button', async () => {
            render(<Button variant="destructive">Destructive</Button>);
            const btn = screen.getByRole('button');
            expect(btn).toMatchSnapshot();
        });

        it('outline button', async () => {
            render(<Button variant="outline">outline</Button>);
            const btn = screen.getByRole('button');
            expect(btn).toMatchSnapshot();
        });

        it('secondary button', async () => {
            render(<Button variant="secondary">secondary</Button>);
            const btn = screen.getByRole('button');
            expect(btn).toMatchSnapshot();
        });

        it('ghost button', async () => {
            render(<Button variant="ghost">ghost</Button>);
            const btn = screen.getByRole('button');
            expect(btn).toMatchSnapshot();
        });

        it('link button', async () => {
            render(<Button variant="link">link</Button>);
            const btn = screen.getByRole('button');
            expect(btn).toMatchSnapshot();
        });

        it('asChild button', async () => {
            render(
                <Button asChild>
                    <Link href="/login">Login</Link>
                </Button>
            );
            const btn = screen.findAllByText('login');
            expect(btn).toMatchSnapshot();
        });

    });

 });
