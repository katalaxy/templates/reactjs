import {
  expect,
  it,
  describe,
} from 'vitest';
import {
  cn,
} from './utils';

describe('cn function', () => {
  it('should merge class names using clsx and twMerge', () => {
    const result = cn('class1', 'class2', null, undefined, 'class3');
    expect(result).toEqual('class1 class2 class3');
  });
});
