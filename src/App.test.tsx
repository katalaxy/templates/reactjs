import {
  expect,
  it,
} from 'vitest';
import {
  render,
  screen,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import '@testing-library/jest-dom/vitest';
import App from './App';

it('renders', async () => {
  const app = render(<App />);
  expect(app).toMatchSnapshot();
});

it('counts on click', async () => {
  const user = userEvent.setup();
  render(<App />);

  const countButton = screen.getByRole('button', {
    name: /^count is \d+$/,
  });

  expect(countButton).toBeInTheDocument();
  expect(countButton.textContent).toBe('count is 0');

  await user.click(countButton);

  expect(countButton.textContent).toBe('count is 1');
});