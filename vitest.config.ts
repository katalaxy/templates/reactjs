import { 
  defineConfig, 
} from "vitest/config";
import path from 'path';

export default defineConfig({
    test: {
        environment: 'happy-dom',
        coverage: {
          provider: 'istanbul',
          reporter: [
            'text-summary',
            'text',
            'html',
          ],
          exclude:[
            'coverage/**',
            'dist/**',
            '**/[.]**',
            'packages/*/test?(s)/**',
            '**/*.d.ts',
            '**/virtual:*',
            '**/__x00__*',
            '**/\x00*',
            'cypress/**',
            'test?(s)/**',
            'test?(-*).?(c|m)[jt]s?(x)',
            '**/*{.,-}{test,spec}.?(c|m)[jt]s?(x)',
            '**/__tests__/**',
            '**/{karma,rollup,webpack,vite,vitest,jest,ava,babel,nyc,cypress,tsup,build}.config.*',
            '**/vitest.{workspace,projects}.[jt]s?(on)',
            '**/.{eslint,mocha,prettier}rc.{?(c|m)js,yml}',
            '**/tailwind.config.js',
            '**/**.stories.tsx'
          ]
        },
        alias: {
            "@": path.resolve(__dirname, "./src"),
        },
        setupFiles: [
          './vitest.setup.ts',
        ]    
      },
})